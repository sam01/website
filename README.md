# Website

This site uses zola, a static site engine, to generate the website files. The current theme in use is tabi.

See this links for further information:

- [zola](https://www.getzola.org/) as the static site engine
- [tabi](https://www.getzola.org/themes/tabi/)

The generated files are in a seperate repository called [pages](https://codeberg.org/sam01/pages/src/branch/main). Codeberg-pages uses this repo to display the website.

Build the static site with zola build into the public directory.

Copy the files from the public directory to the pages repository.
