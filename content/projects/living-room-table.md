+++
title = "Living room table"
date = 2024-04-21
description = "Building a table for our living room"
weight = 1

[extra]
local_image = "thumbs/thumb_IMG_20240424_233537-5.jpg"
link_to = "/blog/living-room-table"
+++
