+++
title = "Polar viewfinder lighting"
date = 2024-05-20
description = "3D design, print and assemble an extension light for a polar viewfinder"
weight = 2

[extra]
local_image = "thumbs/thumb_IMG_20240520_231000.jpg"
link_to = "/blog/polar-viewfinder-lighting"
+++