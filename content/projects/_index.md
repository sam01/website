+++
title = "Projects"
sort_by = "weight"
template = "cards.html"
insert_anchor_links = "left"

[extra]
show_reading_time = true
quick_navigation_buttons = true
hide_from_feed = true
+++