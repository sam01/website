+++
title = "Developing the website theme"
date = 2024-06-14
description = "Develop an option in the tabi-theme"
weight = 3
updated = 2024-07-13

[taxonomies]
tags = ["development", "zola", "tabi"]

[extra]
local_image = "thumbs/thumb-2024-07-13-tabi-theme.jpg"
link_to = "/blog/tabi-theme"
+++
