+++
title = ""
template = "index.html"

[extra]
header = {title = "Oh, hey..", img = "profile-pic.png"}
section_path = "blog/_index.md"
max_posts = 2
projects_path = "projects/_index.md"
+++

Welcome to this corner of the Internet!  
This is my personal website and it's a work in progress obviously 🛠️

My name is Sam Peters and I am interested in a lot of things. I actually always have something to do, my head is constantly throwing out new ideas. Woodworking, electronics, tinkering with computers, trawling the internet, being in love with open source software, biking.. 

My main job is in information technology.
Working with computers of all kinds is usually no problem for me and feels natural. IT security and data protection are part of my job and are very important to me. 
I am convinced that these two things and new, cool technologies are not mutually exclusive, but can be combined. 
