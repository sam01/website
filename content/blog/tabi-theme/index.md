+++
title = "Developing the website theme"
date = 2024-06-14
description = "Develop an option in the tabi-theme"
weight = 2
updated = 2024-07-13

[taxonomies]
tags = ["development", "zola", "tabi"]

[extra]

+++

The static website builder I use works with themes.
I write my content in markdown and [zola](https://www.getzola.org/) (the static site builder) uses [tabi](https://github.com/welpo/tabi) (the theme) to generate the site files. 

Since it's all open source you can fiddle with it and change things as you like.

A few days ago, I added content to one of my blog posts and thought about the best way to present it.  
On a blog post there is the "Creation" date and the "Last updated on" date, so I changed the latter.

Then I noticed that on the start page, on the blog page and in the RSS feed only the creation date is visible and I wondered whether this could be adjusted.

I created a Github issue to talk to the developer: [https://github.com/welpo/tabi/issues/326](https://github.com/welpo/tabi/issues/326)

We exchanged ideas about how to display it nicely and how to incorporate more options into the theme.

After that I've set to work and implemented a way to decide what you want to display with a new option in the config file.  
The new option in the `config.toml` looks like that:

```toml
# There are three options to display dates on the bloglist. The bloglist is used on the frontpage and the blog site.
# Use one of the three options.
# Shows the "Last updated on" date
bloglist_date = "updated"
# Shows the creation date
# bloglist_date = "date"
# Shows both dates
# bloglist_date = "both"
```

I changed the html_template `list_posts.html` to match the options:

```jinja2
<ul class="bloglist-meta">
    {% if post.updated and post.date and config.extra.bloglist_date == "both" %}
        <li class="date"> {{ macros_format_date::format_date(date=post.date, short=false, language_strings=language_strings) }} • Last updated on {{ macros_format_date::format_date(date=post.updated, short=true, language_strings=language_strings) }}</li>
    {% elif post.updated and config.extra.bloglist_date == "updated" %}
        <li class="date"> Last updated on {{ macros_format_date::format_date(date=post.updated, short=false, language_strings=language_strings) }}</li>
    {% elif post.date %}
        <li class="date">{{ macros_format_date::format_date(date=post.date, short=false, language_strings=language_strings) }}</li>
    {% endif %}
    {% if post.draft %}
    <li class="draft-label">{{ macros_translate::translate(key="draft", default="DRAFT", language_strings=language_strings) }}</li>
    {% endif %}
</ul>
```

And added a css instruction to the `bloglist-meta` class to improve the appearance: `margin-right: 0.7rem;`.

---------------

## The final result looks like this:

*See the dates on the left side*

Option `bloglist_date = "updated"`:

![](assets/option1.png)

Option `bloglist_date = "date"`:

![](assets/option2.png)

Option `bloglist_date = "both"`:

![](assets/option3.png)


But this is not the end of the story.  
The RSS feed is still missing and development continues. Stay tuned and look for the updated dates on my site 🚀


## Update on 13th Jul 2024

Óscar, the developer of the theme, and I developed the feature over the last few weeks.  
You can follow the development process in the pull request: https://github.com/welpo/tabi/pull/330

Two days ago, my pull request was merged so that it is now part of the main repo of the theme! Now everyone can use it by simply updating the theme.

### What function does the Feature offer?

By default, when listing posts, the date of post creation is shown. You can configure which date(s) to display using the `post_listing_date` option. Available settings:

- `date`: Show only the original date of the post (default).
- `updated`: Show only the last updated date of the post.
- `both`: Show both the original date and the last updated date.

```toml
# Determines how dates are displayed in the post listing (e.g. front page or /blog). Options:
# "date" - Show only the original date of the post (default if unset).
# "updated" - Show only the last updated date of the post.
# "both" - Show both the original date and the last updated date.
post_listing_date = "updated"
```

This setting changes the displayed dates in a few places:

1. On the main page, if you decide to list the recent posts
2. On the blog-list page
3. On the Atom/RSS feed page

Depending on the screen width, the separator symbol is added with the "both" option.

Full width:

![](assets/option-both-full-width.png)

Mobile view:

![](assets/option-both-mobile.png)


For now I set the "updated" option. It looks clean and adds useful information.

This was a fun little development project! 👩🏽‍💻
